---
title: "About"
date: 2022-01-08T10:41:03+06:00
subTitle: The story of 2600 Thailand
sliderImage:
  - image: "images/stor/exib-01.jpg"
  - image: "images/stor/exib-02.jpg"
  - image: "images/stor/exib-03.jpg"
  - image: "images/stor/exib-04.jpg"
---
## Coalition of Thailand Hacker Spaces.

กลุ่ม [2600 Thailand](https://www.facebook.com/groups/2600Thailand) เป็นกลุ่มผู้สนใจด้านความมั่นคงปลอดภัยไซเบอร์ของประเทศไทย ก่อตั้งขึ้นในวันที่ 28 กุมภาพันธ์ 2013 ภายใต้การเปลี่ยนผ่านจากยุคที่การรวมตัวและแลกเปลี่ยนความรู้กันนั้นเกิดขึ้นในเว็บบอร์ด มาเป็นยุคของการใช้สังคมออนไลน์เป็นหลัก

2600 Thailand รับ[แนวทางการดำเนินการ](https://www.2600.com/meetings/guidelines.html)ของกลุ่มมาจาก [2600 Meetings](https://www.2600.com/meetings/) ซึ่งเป็นส่วนหนึ่งของ [2600: The Hacker Quarterly](https://www.2600.com/)

<aside>
💡 2600 มีที่มาจากการเกิดขึ้นของวัฒนธรรม [Phreaking](https://en.wikipedia.org/wiki/Phreaking) ในยุคช่วง 1960s หลังจากมีการค้นพบว่าหากมีการนำนกหวีดที่ถูกแถมมาในผลิตภัณฑ์อาหารเช้า [Cap'n Crunch](https://en.wikipedia.org/wiki/Cap%27n_Crunch) มาเป่าเมื่อมีการใช้งานโทรศัพท์ นกหวีดดังกล่าวจะสร้างเสียงซึ่งมีความถี่อยู่ที่ 2600 hertz เข้าไปในเครือข่ายโทรศัพท์ ทำให้ผู้ใช้งานสามารถเข้าถึงระบบของเครือข่ายโทรศัพท์ที่ไม่สามารถเข้าถึงได้้วยวิธีการทั่วไปภายใต้สิทธิ์ของผู้ให้บริการได้

</aside>

ด้วยอิทธิพลและวัฒนกรรมของเว็บบอร์ดและการเกิดขึ้นของ Hacker space ในประเทศไทยที่ซึ่งความสนใจในประเด็นที่มีร่วมกันได้ถูกนำมาพูดคุยและแลกเปลี่ยนกัน กลุ่ม 2600 Thailand จึงถูกก่อตั้งขึ้นมาเพื่อคงไว้การเป็นพื้นที่ของการเรียนรู้และแลกเปลี่ยน เป็นพื้นที่ให้ผู้ที่ได้เรียนรู้สิ่งใหม่ในวันนี้กลายเป็นผู้ที่ถ่ายทอดและส่งต่อแก่ผู้อื่นในอนาคต

กลุ่ม 2600 Thailand มีกิจกรรมสัมมนาที่ดำเนินการอยู่เป็นหลัก 2 กิจกรรมได้แก่ [2600 Thailand Meeting ประจำเดือน](https://www.facebook.com/legacy/notes/448400761986624/)และกิจกรรม [Red Pill / Red x Blue Pill Cybersecurity Conference](https://www.facebook.com/legacy/notes/1456733037820053/) ซึ่งเป็นกิจกรรมประจำปี

กลุ่ม 2600 Thailand ยังเป็นกลุ่มหลักของ [OWASP Thailand Chapter](https://www.facebook.com/owaspthailand) และผู้จัดการแข่งขัน [STDiO CTF Competition](https://www.facebook.com/stdioctf) ซึ่งมีเป้าหมายในการคัดเลือกนักศึกษาไทยในการเข้าร่วม [Global Cybersecurity Camp (GCC)](https://gcc.ac/)

กิจกรรม Red x Blue Pill 2022 นับเป็นกิจกรรมประจำปีครั้งที่ 4 ของกลุ่ม 2600 Thailand และเป็นครั้งที่สองของการเปลี่ยนจาก “Red Pill” ซึ่งมุ่งเน้นไปที่การสัมมนาด้าน Offensive cybersecurity เป็นหลัก มาเป็น “Red x Blue Pill” ที่เพิ่มการสัมมนาด้าน Defensive cybersecurity ด้วย
