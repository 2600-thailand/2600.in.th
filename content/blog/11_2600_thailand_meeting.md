---
title: "11th 2600 Thailand Meeting"
date: 2014-08-01T18:00:00+07:00
featureImage: images/blog/11.jpg
tags: ["Monthly Events"]
draft: false
---

Embedded Security CTF (Reverse Engineering) โดย กรินทร์ สุมังคะโยธิน (นักศึกษาปริญญาเอก สถาบันเทคโนโลยีนานาชาติสิรินธร มหาวิทยาลัยธรรมศาสตร์).

See more details [here](https://www.facebook.com/events/665404540214116/)