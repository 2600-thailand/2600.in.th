---
title: "13th 2600 Thailand Meeting"
date: 2014-11-07T18:00:00+07:00
featureImage: https://img.youtube.com/vi/FlfxGFdnCsA/0.jpg
tags: ["Monthly Events"]
draft: false
---

- [Incident Response: Operation Before/After Breach โดย สุเมธ จิตภักดีบดินทร์ (Senior Security Researcher, บริษัท i-Secure จำกัด)](https://www.youtube.com/watch?v=FlfxGFdnCsA)
- Introduction to Cyber Lab: Hacking without going to jail โดย ประสาทพร นิลกำแหง (Security Specialist)

See more details [here](https://www.facebook.com/events/934652399897787/)