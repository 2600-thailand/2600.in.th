---
title: "14th 2600 Thailand Meeting"
date: 2014-12-12T18:00:00+07:00
featureImage: images/blog/logo.jpg
tags: ["Monthly Events"]
draft: false
---

- Introduction to Malware Analysis โดย วรพงษ์ บูรณ์พงษ์ทอง (Student, Researcher at KMITL)
- File Inclusion 101: It is not just about ../etc/passwd โดย กิจวิพัฒน์ โตวัฒนา (บริษัท I-SECURE จำกัด)
- บอกเล่าประสบการณ์ร่วมงาน OWASP Asia Tour 2014

See more details [here](https://www.facebook.com/events/393717817458979/)