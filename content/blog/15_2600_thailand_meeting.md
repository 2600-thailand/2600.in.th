---
title: "15th 2600 Thailand Meeting"
date: 2015-01-16T18:00:00+07:00
featureImage: images/blog/logo.jpg
tags: ["Monthly Events"]
draft: false
---

Activity: Secure Code Review โดย วิศวัชร์ อัศวเมนะกุล (Senior Information Security, บริษัท เน็ตแอสเซส คอนซัลติ้ง จำกัด), พิชญะ โมริโมโตะ (คณะเทคโนโลยีสารสนเทศ สาขาวิทยาการคอมพิวเตอร์, มหาวิทยาลัยเทคโนโลยีพระจอมเกล้าธนบุรี)

See more details [here](https://www.facebook.com/events/1551394861765812/)