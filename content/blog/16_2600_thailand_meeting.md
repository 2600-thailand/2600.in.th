---
title: "16th 2600 Thailand Meeting"
date: 2015-03-06T18:00:00+07:00
featureImage: images/blog/logo.jpg
tags: ["Monthly Events"]
draft: false
---

- Hack in the Mov(ies) โดย ณรงค์ฤทธิ์ สุขสาร
- Unix application stack buffer overflow and exploitation โดย อัมฤทธิ์ ทองทั่ว (Penetration Tester Team Leader)

See more details [here](https://www.facebook.com/events/814573271954467/)