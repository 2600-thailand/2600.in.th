---
title: "2600 Thailand x Thai Netizen x Documentary Club"
date: 2015-03-29T18:00:00+07:00
featureImage: images/blog/16_special.jpg
tags: ["Monthly Events"]
draft: false
---

CitizenFour: หนัง-คอมพ์-คน-พลเมือนดิจิตอลร่วมเสวนา โดย พิชญพงษ์ ตันติกุล, ก้อง ฤทธิ์ดี และ อาทิตย์ สุริยะวงศ์กุล

See more details [here](https://www.facebook.com/events/1605914839639999/)