---
title: "17th 2600 Thailand Meeting"
date: 2015-04-03T18:00:00+07:00
featureImage: images/blog/17.jpg
tags: ["Monthly Events"]
draft: false
---

- [Black Hat Asia 2015 Wrap-up โดย ปิตุพงศ์ ยาวิราช (Information Security Consultant, MFEC Public Company Ltd)](https://www.youtube.com/watch?v=sh1W-eI9n6I)
- [SSL/TLS Overview and Attacks โดย ดร. ภูมิ ภูมิรัตน (นักวิชาการอิสระ)](https://www.youtube.com/watch?v=zyOHWQeoZSk)

See more details [here](https://www.facebook.com/events/914342668612628/)