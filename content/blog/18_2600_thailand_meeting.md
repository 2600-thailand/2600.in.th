---
title: "18th 2600 Thailand Meeting"
date: 2015-05-08T18:00:00+07:00
featureImage: images/blog/logo.jpg
tags: ["Monthly Events"]
draft: false
---

Activity: Secure Code Review โดย วิศวัชร์ อัศวเมนะกุล (Senior Information Security, บริษัท เน็ตแอสเซส คอนซัลติ้ง จำกัด)

See more details [here](https://www.facebook.com/events/639779672823753/)