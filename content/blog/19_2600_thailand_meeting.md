---
title: "19th 2600 Thailand Meeting"
date: 2015-06-05T18:00:00+07:00
featureImage: https://img.youtube.com/vi/HgG1QUM-GUI/0.jpg
tags: ["Monthly Events"]
draft: false
---

- [Quantum Cryptography: Alice and Bob in the Quantum World โดย วรพงษ์ บูรณ์พงษ์ทอง (Student, Researcher at KMITL)](https://www.youtube.com/watch?v=HgG1QUM-GUI)
- [Race Condition 101 โดย พิชญะ โมริโมโตะ (คณะเทคโนโลยีสารสนเทศ สาขาวิทยาการคอมพิวเตอร์, มหาวิทยาลัยเทคโนโลยีพระจอมเกล้าธนบุรี)](https://www.youtube.com/watch?v=9rsKSSERYXw)

See more details [here](https://www.facebook.com/events/1610537639227713/)