---
title: "1st 2600 Thailand Meeting"
date: 2013-04-05T18:00:00+07:00
featureImage: https://img.youtube.com/vi/NFTuX_jHnsw/0.jpg
tags: ["Monthly Events"]
draft: false
---

- [2600 Thailand คืออะไร? โดย กรินทร์ สุมังคะโยธิน (2600 Thailand Founder)](https://www.youtube.com/watch?v=NFTuX_jHnsw)
- [Hacking from Network Device Down to OS โดย กริช กาศนอก](https://www.youtube.com/watch?v=np9a-ZWAV7o)
