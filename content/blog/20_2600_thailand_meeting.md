---
title: "20th 2600 Thailand Meeting"
date: 2015-08-07T18:00:00+07:00
featureImage: images/blog/logo.jpg
tags: ["Monthly Events"]
draft: false
---

- Point-of-Sale Hacking โดย ประธาน พงศ์ทิพย์ฤกษ์ (Security Consultant, KPMG)
- What should I do when my website got hack? โดย สุเมธ จิตภักดีบดินทร์ (Senior Security Research, บริษัท i-Secure จำกัด)

See more details [here](https://www.facebook.com/events/409770605896405/)