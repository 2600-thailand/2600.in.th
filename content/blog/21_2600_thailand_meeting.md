---
title: "21st 2600 Thailand Meeting"
date: 2015-09-21T18:00:00+07:00
featureImage: images/blog/logo.jpg
tags: ["Monthly Events"]
draft: false
---

- [RSA Conference Asia Briefing โดย อัมฤทธิ์ ทองทั่ว (Infosec Consultant and Penetration Tester)](https://www.facebook.com/events/1489486378033564/)
- [Memory Forensics โดย สุรทศ ไตรติลานันท์ (คณะวิศวกรรมคอมพิวเตอร์ มหาวิทยาลัยมหิดล)](https://www.facebook.com/events/1489486378033564/)
