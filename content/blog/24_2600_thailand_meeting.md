---
title: "24th 2600 Thailand Meeting"
date: 2016-06-03T18:00:00+07:00
featureImage: images/blog/logo.jpg
tags: ["Monthly Events"]
draft: false
---

- [Use the CRACK, they said. It will be FUN, they said. โดย Worapong Boonpongthong](https://www.facebook.com/events/245582595832607/)
- [Hypervisor Vulnerability Assessment - การทดสอบเจาะแฮกช่องโหว่บนตัวจัดการ Virtual Machine (VM) โดย อัมฤทธิ์ ทองทั่ว (IT Security Specialist, Agoda)](https://www.facebook.com/events/245582595832607/)