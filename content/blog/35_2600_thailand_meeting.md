---
title: "35th 2600 Thailand Meeting"
date: 2018-02-09T18:00:00+07:00
featureImage: images/blog/logo.jpg
tags: ["Monthly Events"]
draft: false
---

- [Burp Extender API for Penetration Testing โดย พิชญะ โมริโมโตะ (IT Security Consultant, SEC Consult (Thailand) Co., Ltd.)](https://web.facebook.com/events/1888062704839184/)
- [ Network Security Monitoring (English session) โดย William Sandin, 0xEBFE](https://web.facebook.com/events/1888062704839184/)
