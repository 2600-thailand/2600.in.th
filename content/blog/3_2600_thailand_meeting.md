---
title: "3rd 2600 Thailand Meeting"
date: 2013-07-05T18:00:00+07:00
featureImage: https://img.youtube.com/vi/lQVY1IM7XLA/0.jpg
tags: ["Monthly Events"]
draft: false
---

- [Hash cracking and Bitcoin mining with the Monster GPUs โดย พัฐวงศ์ พันธ์โสตถี (นักศึกษา มหาวิทยาลัยพระจอมเกล้าพระนครเหนือ)](https://www.youtube.com/watch?v=lQVY1IM7XLA)
- [Entrepreneur vs Information Security "ทัศนะด้านความปลอดภัยสารสนเทศในมุมมองของผู้ประกอบการ" โดย วรวุฒิ สายบัว (กรรมการผู้จัดการ, บริษัท เอ็นเซ็ก ไอที คอนเวอร์เจนซ์ กรุ๊ป จำกัด)](https://www.youtube.com/watch?v=5OUYAwfFoBk)
- [เรียนรู้ Computer Security อย่างไรไม่ให้ผิดกฎหมาย (Discussion) โดย ณรงค์ฤทธิ์ สุขสาร (โปรแกรมเมอร์ บริษัท ไทยไซเบอร์เกมส์), พันตำรวจโท ดรัณ จาดเจริญ และ กิติศักดิ์ จิรวรรณกูล](https://www.youtube.com/watch?v=Y5f4CCdnSmA)
