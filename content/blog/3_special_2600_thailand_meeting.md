---
title: "2600 Thailand Meeting Special Session with OWASP Thailand"
date: 2013-06-07T18:00:00+07:00
featureImage: https://img.youtube.com/vi/9v10SlzQysk/0.jpg
tags: ["Monthly Events"]
draft: false
---

- [OWASP + 2600 คืออะไร โดย กิติศักดิ์ จิรวรรณกูล (OWASP Thailand Founder) และ กรินทร์ สุม้งคะโยธิน (2600 Thailand Founder)](https://www.youtube.com/playlist?list=PLUy5CmhyDowit5d3k_sZg_FrxMjdDRM1J)
- ความสำคัญกับการศึกษาด้าน Security โดย พัฒนพล รัตนพงษ์พร, ทิวากร แตงอ่อน, พัฐวงศ์ พันธ์โสตถีและกรินทร์ สุมังคะโยธิน
- ช่องโหว่ของเว็บที่ผู้พัฒนาต้องใส่ใจ โดย ประธาน พงศ์ทิพย์ฤกษ์ และ กริช กาศนอก
