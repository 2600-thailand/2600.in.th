---
title: "4th 2600 Thailand Meeting"
date: 2013-08-02T18:00:00+07:00
featureImage: https://img.youtube.com/vi/cQnoRhub9MY/0.jpg
tags: ["Monthly Events"]
draft: false
---

- [All about of SQL Injection and WAF bypass Technique โดย ทัศนัย เดชดำรงปรีชา (สถาบันเทคโนโลยีพระจอมเกล้าเจ้าคุณทหารลาดกระบัง)](https://www.youtube.com/watch?v=cQnoRhub9MY)
- [Introduction to Malware Analysis with case study โดย วิศวัชร์ อัศวเมนะกุล (Senior Information Security, บริษัท เน็ตแอสเซส คอนซัลติ้ง จำกัด)](https://www.youtube.com/watch?v=p4wa_F4JnQg)
- [ภัยจากโปรแกรมโกงเกมส์และโปรแกรม hack โดย ณรงค์ฤทธิ์ สุขสาร (โปรแกรมเมอร์ บริษัท ไทยไซเบอร์เกมส์)](https://www.youtube.com/watch?v=nUX-EuD4rdI)
