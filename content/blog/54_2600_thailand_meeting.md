---
title: "54th 2600 Thailand Meeting"
date: 2023-04-28T18:00:00+07:00
featureImage: images/blog/logo.jpg
tags: ["Monthly Events"]
draft: false
---

Windows Local Privilege Escalation by Mr. Chairat Toraya, Information Security Engineer, MFEC PCL  

Slides Download Link: [Download](https://www.facebook.com/download/677377757156870/2600_Thailand_Meeting_54_Windows_LPE_2023-04-28.pptx?av=1676117685&eav=AfbIQ24itjQ_pQX9EitpgoXnitvG-kLuFO-X2FSvez8lKyZlAk8m7LvfoMcTZXGMlMY&paipv=0&hash=Acpw3oK95E1uy6RoI68)

See more details [here](https://www.facebook.com/events/930883068144668/)

