---
title: "5th 2600 Thailand Meeting"
date: 2013-09-06T18:00:00+07:00
featureImage: https://img.youtube.com/vi/A-fs4uTV6Bg/0.jpg
tags: ["Monthly Events"]
draft: false
---

- [**ck, choose it. โดย สุเมธ จิตภักดีบดินทร์ (Senior Security Researcher, ACIS i-Secure)](https://www.youtube.com/watch?v=A-fs4uTV6Bg)
- [Penetration testing โดย ชาคริต แสนบัวโพธิ์ (Senior Information Security, MFEC Public Company Ltd.)](https://www.youtube.com/watch?v=lNeFQo2dWys)
- [Art of Web Backdoor: stealth ways to hide your ass in pwned box โดย พิชญะ โมริโมโตะ (คณะเทคโนโลยีสารสนเทศ สาขาวิทยาการคอมพิวเตอร์, มหาวิทยาลัยเทคโนโลยีพระจอมเกล้าธนบุรี)](https://www.youtube.com/watch?v=QIXTPPBfLyIA)
