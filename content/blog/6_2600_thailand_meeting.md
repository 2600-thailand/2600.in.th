---
title: "6th 2600 Thailand Meeting"
date: 2013-10-04T18:00:00+07:00
featureImage: https://img.youtube.com/vi/d_halWaqDEc/0.jpg
tags: ["Monthly Events"]
draft: false
---

- [Panel Discussion: เกี่ยวกับความพร้อมของประเทศไทยต่อสงครามไซเบอร์ของบุคลากรจากหน่วยงานภายใต้กระทรวงกลาโหม โดย เรือโท ภัธ โชติกะพุกกะนะ (นายทหารรักษาความปลอดภัยคอมพิวเตอร์ ศูนย์รักษาความปลอดภัยคอมพิวเตอร์ กรมเทคโนโลยีสารสนเทศและอวกาศกลาโหมกระทรวงกลาโหม)](https://www.youtube.com/watch?v=d_halWaqDEc)
- [How to build your own WARROOM โดย ปิตุพงศ์ ยาวิราช (นักวิจัยอิสระ)](https://www.youtube.com/watch?v=ZHMm2BUg2o4)
- [Security issues in Verhicular Adhoc Network โดย กรินทร์ ุมังคะโยธิน (อาจารย์พิเศษและนักศึกษาปริญญาเอก ด้าน VANET Security)](https://www.youtube.com/watch?v=IuduHfcdGX8)
