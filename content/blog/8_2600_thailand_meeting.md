---
title: "8th 2600 Thailand Meeting"
date: 2013-12-13T18:00:00+07:00
featureImage: https://img.youtube.com/vi/bnZs7K_EgPU/0.jpg
tags: ["Monthly Events"]
draft: false
---

- [แนะแนวการศึกษาต่อในสาขา computer security ในประเทศ โดย สุเมธ จิตภักดีบดินทร์ (Senior Security Researcher, ACIS i-Secure)](https://www.youtube.com/watch?v=bnZs7K_EgPU)
- [แนะแนวการศึกษาต่อในสาขา computer security ในต่างประเทศ โดย ญาณรักข์ วรรณสาย (IT Security Analyst, Kimberly-Clark Corporation)](https://www.youtube.com/watch?v=j-ATHQkCaDo)
- [Certificate ทางด้าน Computer security นั้น สำคัญไฉน? โดยสุ รชัย ฉัตรเฉลิมพันธุ์ (หัวหน้าทีม IT Security, บริษัทหลักทรัพย์ เมย์แบงค์ กิมเอ็ง ประเทศไทย จำกัด มหาชน), วิศวัชร์ อัศวเมนะกุล (Senior Information Security, บริษัท เน็ตแอสเซส คอนซัลติ้ง จำกัด) และ ประธาน พงศ์ทิพย์ฤกษ์ (Security Consultant)](https://www.youtube.com/watch?v=yoaRNxlpm9Y)
