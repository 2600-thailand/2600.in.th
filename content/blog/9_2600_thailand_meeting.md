---
title: "9th 2600 Thailand Meeting"
date: 2014-06-06T18:00:00+07:00
featureImage: https://img.youtube.com/vi/oIC7ZmtqImU/0.jpg
tags: ["Monthly Events"]
draft: false
---

- [Introduction To Game.Rop.Sh - เกมแนวลับสมองเพื่อฝึกทักษะในการแฮกอย่างไม่ผิดกฎหมาย โดย พิชญพงษ์ ตันติกุล](https://www.youtube.com/watch?v=oIC7ZmtqImU)
