---
title: "GCC 2023 - Thank You"
date: 2023-03-07T16:56:00+07:00
featureImage: images/blog/gcc_2023_sg_banner.png
tags: ["Event", "Partnership"]
draft: false
---

GCC 2023 was a great success! Thank you for the contributions from our member organizations, including Security Camp Committee 🇯🇵, Division Zero - Singapore Cybersecurity Community 🇸🇬, 한국정보기술연구원 'Best of the Best' 🇰🇷, AIS3 新型態資安暑期課程 🇹🇼, 2600 Thailand 🇹🇭, VNSecurity 🇻🇳, UQ (University of Queensland) Cyber Security 🇦🇺, and NanoSec 🇲🇾.

It was such a pleasure to contribute to our joint commitment. Looking forward to future collaborations at #GlobalCybersecurityCamp 2024!

See the images [here](https://www.facebook.com/2600Thailand/posts/pfbid0gChXZFf3tGVrTVASpKb2gixbvx72nQGU2qdnWPFbJZJuPytczvtfgMkEe81ejE75l)!
