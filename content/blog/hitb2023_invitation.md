---
title: "HITBSecConf2023 - Phuket"
date: 2023-06-05T14:34:00+07:00
featureImage: images/blog/hitb2023.jpg
tags: ["Event", "Conference", "Partnership"]
draft: false
---

🤝 2600 Thailand x #HITBSecConf2023 by Hack In The Box 🤝

WELCOME Hack In The Box and #HITBSecConf!

2600 Thailand is proud to be a part of the Hack in the Box Security Conference 2023 as a local cybersecurity community and partner.

2600 Thailand would like to invite local researchers and companies to be part of "Community & Security Track (CommSec)" and "Exhibition & Demos"

The CommSec Call for Papers is now open for anyone interesting to share their research at #HITBSecConf2023.

See more details at https://cfp.hackinthebox.org/events/hitb2023hkt-commsec/  

If your company is interested to be part of "Exhibition & Demos". Please reach out to us at contact[@]2600.in[.]th

HITBSecConf2023 will be held at Four Points by Sheraton Phuket, Thailand, between Auguest 21 - 25. See more details at https://conference.hitb.org/hitbsecconf2023hkt/